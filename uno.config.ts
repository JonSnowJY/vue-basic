// uno.config.ts
import {
  defineConfig,
  presetUno,
  presetAttributify,
  presetTagify,
  presetIcons,
  presetWebFonts,
  presetTypography,
  presetMini,
  presetWind,
  transformerDirectives,
  transformerVariantGroup,
  transformerCompileClass,
  transformerAttributifyJsx,
} from 'unocss'
import presetRemToPx from '@unocss/preset-rem-to-px'

export default defineConfig({
  presets: [
    presetUno(),
    presetAttributify(),
    presetTagify(),
    presetIcons(),
    presetWebFonts(),
    presetTypography(),
    presetMini(),
    presetWind(),
    presetRemToPx({ baseFontSize: 16 }),
  ],
  transformers: [
    transformerDirectives(),
    transformerVariantGroup(),
    transformerCompileClass(),
    transformerAttributifyJsx(),
  ],
  shortcuts: [
    ['wh-full', 'w-full h-full'],
    ['ft', 'flex flex-col'],
    ['ftb', 'flex flex-col justify-between'],
    ['fr', 'flex flex-row-reverse'],
    ['frc', 'flex items-center flex-row-reverse'],
    ['fxc', 'flex justify-center'],
    ['fyc', 'flex items-center'],
    ['fb', 'flex justify-between'],
    ['fbc', 'flex justify-between items-center'],
    ['fcc', 'flex justify-center items-center'],
    ['ftcc', 'flex flex-col justify-center items-center'],
    ['ftxc', 'flex flex-col justify-center'],
    ['ftyc', 'flex flex-col items-center'],
  ],
  rules: [[/^wh-(\d+px)$/, ([, d]) => ({ width: `${d}`, height: `${d}` })]],
  theme: {
    colors: {
      primary: 'var(--van-primary-color)',
      success: 'var(--van-success-color)',
      warning: 'var(--van-warning-color)',
      error: 'var(--van-danger-color)',
      'text-1': 'var(--text-color-primary)',
      'text-2': 'var(--text-color-regular)',
      'text-3': 'var(--text-color-secondary)',
    },
  },
})
