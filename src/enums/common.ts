export enum EntrustStatusEnum {
  // 发布中
  PUBLISHING = 1,
  // 已取消
  CANCELLED = 2,
}

export enum OrderStatusEnum {
  // 未接单
  NOT_ACCEPTED = 1,
  // 已接单
  ACCEPTED = 2,
  // 拒绝
  REJECTED = 3,
  // 接单失败
  ORDER_FAILED = 4,
}

export enum ResourceTypeEnum {
  // 资源池
  RESOURCE_POOL = 1,
  // 委托邀请
  ENTRUST_INVITATION = 2,
}

export enum AgreementStatus {
  // 未同意
  NOT_AGREED = 1,
  // 已同意
  AGREED = 2,
}
