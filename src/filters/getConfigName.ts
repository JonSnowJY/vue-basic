export type GetConfigName = (
  config: readonly { name: string; value: number }[],
  value: number | string | undefined,
) => string

const getConfigName: GetConfigName = (config, value) => {
  return config.find((item) => item.value === value)?.name ?? ''
}

export default getConfigName
