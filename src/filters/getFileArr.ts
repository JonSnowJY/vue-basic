export type GetFileArr = (filesStr: string) => FileType[]

const getFileArr: GetFileArr = (filesStr: string) => {
  if (!filesStr) return []
  return JSON.parse(filesStr)
}

export default getFileArr
