import type { App } from 'vue'

import type { CustomFilter } from '../../types/filters.d'

type FilterType = Record<string, any>

const filterFuncs: FilterType = {}
const files: FilterType = import.meta.glob('./*.ts', { eager: true })

Object.keys(files).forEach((fileName) => {
  const fileFunc = files[fileName]
  if (fileFunc && fileFunc.default) {
    const name = fileName.replace(/\.\/|\.ts/g, '')
    filterFuncs[name] = fileFunc.default
  }
})

export const setupGlobFilters = (app: App) => {
  app.config.globalProperties.$filters = filterFuncs as CustomFilter
}
