import { env } from '@/types/helper'

export default {
  SYSTEM_TITLE: env.VITE_APP_TITLE,

  API_URL: env.VITE_API_URL,

  TIMEOUT: 60000,

  STORAGE_PREFIX: 'fxer',

  TOKEN_NAME: 'mt-token',

  UNDERTAKE_ERROR: '未知错误，请联系管理员',

  FILE_IMAGE_SIZE_LIMIT: 50,

  FILE_VIDEO_SIZE_LIMIT: 300,

  PAGE_SIZE: 10,

  DIALOG_WIDTH: 900,

  EMPTY_IMAGE_SIZE: 150,

  REQUIRE_LOGIN_MESSAGE: '请登录',
}
