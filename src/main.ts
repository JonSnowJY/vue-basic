import './assets/scss/main.scss'
import 'virtual:uno.css'
import '@unocss/reset/eric-meyer.css'
import 'element-plus/es/components/message/style/css'

import { createApp } from 'vue'

import { setupGlobDirectives } from '@/directives/index'
import { setupGlobFilters } from '@/filters/index'
import setupPlugin from '@/plugins/index'

import App from './App.vue'
import router from './router'

import sensors from 'sa-sdk-javascript'
sensors.init({
  // server_url: 'http://shence.ap-ec.cn:8106/sa?project=beiyong3',
  server_url: 'https://family.demo.sensorsdata.cn?project=EbizDemo&product=sbp_family&id=413&dash_type=lego',
  // server_url: 'http://shence.ap-ec.cn:8106/sa?project=TelecomCarrierDemo',
  is_track_single_page: true, // 单页面配置，默认开启，若页面中有锚点设计，需要将该配置删除，否则触发锚点会多触发 $pageview 事件
  use_client_time: true,
  use_app_track: true,
  send_type: 'beacon',
  show_log: true,
  heatmap: {
    //是否开启点击图，default 表示开启，自动采集 $WebClick 事件，可以设置 'not_collect' 表示关闭。
    clickmap: 'default',
    //是否开启触达图，not_collect 表示关闭，不会自动采集 $WebStay 事件，可以设置 'default' 表示开启。
    // 页面停留4S以上，滚动页面，触发$WebStay 事件
    scroll_notice_map: 'default',
    collect_tags: {
      div: true,
    },
  },
})
// 注册公共属性
sensors.registerPage({
  appName: '项目名称',
  appType: 'PC',
})
// $pageview
sensors.quick('autoTrack') //用于采集 $pageview 事件。

const app = createApp(App)
app.config.globalProperties.$sensors = sensors

app.use(router)

// 全局插件
setupPlugin(app)
// 全局指令
setupGlobDirectives(app)
// 全局过滤器
setupGlobFilters(app)

app.mount('#app')
