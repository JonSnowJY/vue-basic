import * as CommonConfig from '@/config/common'
type sortOrder = MyUtils.ConfigValue<typeof CommonConfig.sortOrderConfig>

export interface ApiResponseData<T> {
  code: number
  data: T
  message?: string
  msg?: string
}

export interface ApiPageResponseData<T> {
  total: number
  size: number
  current: number
  pages: number
  records: T[]
}

export interface PageParams {
  current: number
  size: number
}

export interface CommonPageParams extends PageParams {
  sortOrder: sortOrder
  sortField: string
}

export interface PageItemCommonFields {
  createTime: string
  createUser: string
  updateTime: string
  updateUser: string
  deleteFlag: string
}
