class Helper {
  public env = import.meta.env
  constructor() {
    this.env = this.getEnvs()
  }

  private getEnvs() {
    const envs: ImportMetaEnv = { ...this.env }
    Object.entries(envs).forEach(([key, value]) => {
      if (['true', 'false'].includes(value)) envs[key] = value === 'true' ? true : false
      else if (/^\d+$/gi.test(value)) envs[key] = +value
      else if (value === 'null') envs[key] = null
      else if (value === 'undefined') envs[key] = undefined
    })
    return envs
  }
}

const helper = new Helper()
const env = helper.env
export default helper

export { env }
