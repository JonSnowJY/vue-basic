import type { App } from 'vue'

type DirectiveType = Record<string, any>
const files: DirectiveType = import.meta.glob('./*.ts', { eager: true })

export const setupGlobDirectives = (app: App) => {
  Object.keys(files).forEach((fileName) => {
    const directiveFunc = files[fileName]
    if (directiveFunc && directiveFunc.default) {
      const key = fileName.replace(/^\.\/|\.ts/g, '')
      app.directive(key, directiveFunc.default)
    }
  })
}
