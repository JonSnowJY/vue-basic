import type { ObjectDirective } from 'vue'

const tooltip: ObjectDirective = {
  beforeMount() {},
  mounted(el: HTMLSpanElement) {
    // 创建
    const dom = document.createElement('span')
    dom.innerText = el.innerText
    document.body.insertAdjacentElement('beforeend', dom)
    const { width } = dom.getBoundingClientRect()
    // 比对
    if (width > el.offsetWidth) {
      el.setAttribute('title', el.innerText)
    }
    // 删除
    dom.remove()
  },
  beforeUpdate() {},
  updated() {},
  beforeUnmount() {},
  unmounted() {},
}

export default tooltip
