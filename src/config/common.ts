import { AgreementStatus, OrderStatusEnum, ResourceTypeEnum } from '@/enums/common'

export const sortOrderConfig = [
  {
    name: '升序',
    value: 'ASCEND',
  },
  {
    name: '降序',
    value: 'DESCEND',
  },
] as const

export const entrustStatusConfig = [
  {
    name: '发布中',
    value: 1,
  },
  {
    name: '已取消',
    value: 2,
  },
] as const

export const lawyerPerformanceConfig = [
  {
    name: '未完成',
    value: 1,
  },
  {
    name: ' 已完成',
    value: 2,
  },
] as const

export const orderStatusConfig = [
  {
    name: '未接单',
    value: OrderStatusEnum.NOT_ACCEPTED,
  },
  {
    name: '已接单',
    value: OrderStatusEnum.ACCEPTED,
  },
  {
    name: '拒绝',
    value: OrderStatusEnum.REJECTED,
  },
  {
    name: '接单失败',
    value: OrderStatusEnum.ORDER_FAILED,
  },
] as const

export const resourceTypeConfig = [
  {
    name: '资源池',
    value: ResourceTypeEnum.RESOURCE_POOL,
  },
  {
    name: '委托邀请',
    value: ResourceTypeEnum.ENTRUST_INVITATION,
  },
] as const

export const agreementConfig = [
  {
    name: '未同意',
    value: AgreementStatus.NOT_AGREED,
  },
  {
    name: '已同意',
    value: AgreementStatus.AGREED,
  },
] as const
