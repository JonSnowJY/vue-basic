import JsCookie from 'js-cookie'

import projectSetting from '@/settings/projectSetting'

class Cookie {
  private static keyFormat(key: string): string {
    return `${projectSetting.STORAGE_PREFIX}-${key}`
  }

  public get(key: string): string | undefined {
    return JsCookie.get(Cookie.keyFormat(key))
  }

  public set(key: string, val: string): void {
    JsCookie.set(Cookie.keyFormat(key), val)
  }

  public remove(key: string): void {
    JsCookie.remove(Cookie.keyFormat(key))
  }

  public clear(): void {
    const allCookies = JsCookie.get()
    Object.keys(allCookies).forEach((key) => {
      JsCookie.remove(key)
    })
  }
}

export default new Cookie()
