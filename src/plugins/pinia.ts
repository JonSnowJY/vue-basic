import { createPinia } from 'pinia'
import piniaPluginPersistedState from 'pinia-plugin-persistedstate'
import { type App } from 'vue'

export default function setupPinia(app: App) {
  const pinia = createPinia()
  pinia.use(piniaPluginPersistedState)
  app.use(pinia)
}
