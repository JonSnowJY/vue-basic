import type { App } from 'vue'

import setupPinia from './pinia'

export default function setupPlugins(app: App) {
  setupPinia(app)
}
