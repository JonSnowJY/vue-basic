import projectSetting from '@/settings/projectSetting'

import Axios from './Axios'
const http = new Axios({ baseURL: projectSetting.API_URL, timeout: projectSetting.TIMEOUT })

export default http
