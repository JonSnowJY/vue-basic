import type { GetConfigName } from '../src/filters/getConfigName'
import type { GetFileArr } from '../src/filters/getConfigName'

export {}

export type CustomFilter = {
  getConfigName: GetConfigName
  getFileArr: GetFileArr
}

declare module 'vue' {
  interface ComponentCustomProperties {
    $filters: CustomFilter
  }
}
