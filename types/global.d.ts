interface ResponseResult<T> {
  code: number
  msg: string
  data: T
}

interface FileType {
  name: string
  url: string
}
