declare namespace MyUtils {
  // 从配置文件中，取出具体值的联合类型
  type ConfigValue<T> = (T extends readonly { name: string; value: infer V }[] ? V : never) | ''

  type TrueOrFalse = true | false

  // 将指定属性设置成必填，未指定的属性设置成选填
  type PartialRequired<T, K extends keyof T> = {
    [P in K]-?: T[P]
  } & {
    [P in Exclude<keyof T, K>]?: T[P]
  }

  // 数字或者空字符串
  type NumberOrEmptyString = string | ''
}
