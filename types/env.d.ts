interface ImportMetaEnv {
  VITE_APP_TITLE: string
  VITE_API_URL: string
  VITE_NUM: number
}
