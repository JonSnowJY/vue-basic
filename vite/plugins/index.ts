import vue from '@vitejs/plugin-vue'
import vueJsx from '@vitejs/plugin-vue-jsx'
import UnoCSS from 'unocss/vite'
import Icons from 'unplugin-icons/vite'
import { type PluginOption } from 'vite'
import Inspect from 'vite-plugin-inspect'
import VueDevTools from 'vite-plugin-vue-devtools'

import setupAutoImport from './autoImport'
import setupComponents from './components'
import setupCompression from './compression'
import setupVisualizer from './visualizer'
import setupZipPack from './zipPack'
import setupHtml from './html'

export default function setupPlugins(isBuild: boolean, env: ImportMetaEnv): PluginOption[] {
  const plugins: PluginOption[] = [
    VueDevTools(),
    vue({
      reactivityTransform: true,
    }),
    vueJsx(),
    UnoCSS(),
    Icons({
      autoInstall: true,
    }),
    Inspect(),
  ]

  plugins.push(
    setupComponents(),
    setupAutoImport(),
    setupCompression(),
    setupVisualizer(),
    setupZipPack(env),
    setupHtml(env),
  )
  return plugins
}
