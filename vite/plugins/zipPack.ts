import zipPack from 'vite-plugin-zip-pack'

export default function setupZipPack(env: ImportMetaEnv) {
  return zipPack({
    inDir: 'dist',
    outDir: './',
    outFileName: `${env.VITE_BUILD_OUT_ZIP_NAME ?? 'dist'}.zip`,
  })
}
