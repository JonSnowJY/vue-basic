import { createHtmlPlugin } from 'vite-plugin-html'

export default function setupHtml(env: ImportMetaEnv) {
  const injectData: Record<string, any> = { title: env.VITE_APP_TITLE, injectScript: '' }
  if (env.VITE_PLATFORM === 'zlb') {
    const scriptList = [
      {
        src: '//assets.zjzwfw.gov.cn/assets/ZWJSBridge/1.1.0/zwjsbridge.js',
        type: 'text/javascript',
      },
      {
        src: '//assets.zjzwfw.gov.cn/assets/zwlog/1.0.0/zwlog.js',
        type: 'text/javascript',
      },
      {
        src: '//appx/web-view.min.js',
        type: 'text/javascript',
      },
    ]
    injectData.injectScript = scriptList.reduce((acc, cur) => {
      acc += `<script src="${cur.src}" type="${cur.type}"></script>`
      return acc
    }, '')
  }
  return createHtmlPlugin({
    minify: true,
    entry: 'src/main.ts',
    template: 'index.html',
    inject: {
      data: injectData,
      // tags: [
      //   {
      //     injectTo: 'body-prepend',
      //     tag: 'div',
      //     attrs: {
      //       id: 'tag',
      //     },
      //   },
      // ],
    },
  })
}
