import Compression from 'vite-plugin-compression'

export default function setupCompression() {
  return Compression({ verbose: true, disable: false, threshold: 10240, algorithm: 'gzip', ext: '.gz' })
}
