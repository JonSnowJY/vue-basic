import { visualizer } from 'rollup-plugin-visualizer'

export default function setupVisualizer() {
  return visualizer({ emitFile: false, filename: 'stats.html', open: true })
}
