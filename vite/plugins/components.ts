import IconsResolver from 'unplugin-icons/resolver'
import { ElementPlusResolver } from 'unplugin-vue-components/resolvers'
import Components from 'unplugin-vue-components/vite'

export default function setupComponents() {
  return Components({
    dirs: ['src/components'],
    dts: './types/components.d.ts',
    resolvers: [
      IconsResolver({
        enabledCollections: ['ep'],
      }),
      ElementPlusResolver(),
    ],
  })
}
