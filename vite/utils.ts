export function parseEnv(env: Record<string, string>) {
  const envs: Record<string, any> = { ...env }
  Object.entries(env).map(([key, value]) => {
    if (['true', 'false'].includes(value)) {
      envs[key] = value === 'true' ? true : false
    } else if (/^\d+$/.test(value)) {
      envs[key] = +value
    }
  })
  return envs as ImportMetaEnv
}
